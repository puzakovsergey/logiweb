package com.javaschool.service;

import com.javaschool.dao.OrderCrudRepository;
import com.javaschool.entity.order.OrderEntity;
import com.javaschool.model.WebOrder;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.Collections;
import java.util.HashSet;
import java.util.Set;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class OrderServiceImplTest {

    @Mock
    private OrderCrudRepository orderCrudRepository;

    @InjectMocks
    private OrderServiceImpl orderService;

    @Test
    public void testFindAll() {
        Set<OrderEntity> testSet = new HashSet<>();
        testSet.add(new OrderEntity());
        when(orderCrudRepository.findAll()).thenReturn(testSet);
        Set<WebOrder> all = orderService.findAll();
        assertEquals(1, all.size());
    }


    @Test
    public void findById() {
        Long id = 1L;
        when(orderCrudRepository.findById(id)).thenReturn(java.util.Optional.of(new OrderEntity()));
        WebOrder order = orderService.findById(id);
        assertEquals(WebOrder.class, order.getClass());
    }


    @Test
    public void deleteById() {
    }

    @Test
    public void updateDriverById() {
    }

    @Test
    public void updateTruckById() {
    }

    @Test
    public void addGoodsById() {
    }

    @Test
    public void removeGoodsById() {

    }

    @Test
    public void getWaypointsListById() {
    }

    @Test
    public void getGoodsListById() {
    }

    @Test
    public void getFreeDriversSetById() {
    }

    @Test
    public void getOrdersSetByDriver() {
    }

    @Test
    public void computeDeliveryTimeAndMaxWeight() {
    }

    @Test
    public void computeDeliveryTimeAndMaxWeight2() {
    }

    @Test
    public void save() {
    }
}