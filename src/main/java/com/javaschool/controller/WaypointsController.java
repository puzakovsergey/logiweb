package com.javaschool.controller;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.javaschool.model.WebOrder;
import com.javaschool.model.WebWaypoint;
import com.javaschool.service.GoodsService;
import com.javaschool.service.OrderService;
import org.springframework.dao.DataAccessException;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping(path = "/api/waypoints", produces = MediaType.APPLICATION_JSON_VALUE)
public class WaypointsController {
    private OrderService orderService;
    private ObjectMapper objectMapper;

    public WaypointsController(OrderService orderService, ObjectMapper objectMapper) {
        this.orderService = orderService;
        this.objectMapper = objectMapper;
    }

    @PutMapping("/set_waypoint_reached")
    public ResponseEntity<String> setWaypointReached(@RequestBody WebWaypoint waypoint) {
        try {
            waypoint = orderService.setWaypointReached(waypoint);
            return new ResponseEntity<>(objectMapper.writeValueAsString(waypoint), HttpStatus.OK);
        } catch (JsonProcessingException e) {
            return new ResponseEntity<>("Json mapper exception", HttpStatus.INTERNAL_SERVER_ERROR);
        } catch (DataAccessException e) {
            return new ResponseEntity<>("DataBase exception", HttpStatus.BAD_REQUEST);
        } catch (IllegalArgumentException e) {
            return new ResponseEntity<>(e.getMessage(), HttpStatus.BAD_REQUEST);
        }
    }

    @PutMapping("/get_waypoints_list_for_order")
    public ResponseEntity<String> getWaypointsListForOrder(@RequestBody WebOrder order) {
        try {
            List<WebWaypoint> waypoints = orderService.getWaypointsList(order);
            return new ResponseEntity<>(objectMapper.writeValueAsString(waypoints), HttpStatus.OK);
        } catch (JsonProcessingException e) {
            return new ResponseEntity<>("Json mapper exception", HttpStatus.INTERNAL_SERVER_ERROR);
        } catch (DataAccessException e) {
            return new ResponseEntity<>("DataBase exception", HttpStatus.BAD_REQUEST);
        } catch (IllegalArgumentException e) {
            return new ResponseEntity<>(e.getMessage(), HttpStatus.BAD_REQUEST);
        }
    }
}
