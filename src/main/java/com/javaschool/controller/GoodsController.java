package com.javaschool.controller;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.javaschool.entity.order.goods.GoodsEntity;
import com.javaschool.model.WebGood;
import com.javaschool.service.GoodsService;
import com.javaschool.service.utils.GoodConverter;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.Set;

@RestController
@RequestMapping(path = "/api/goods", produces = MediaType.APPLICATION_JSON_VALUE)
public class GoodsController {
    private GoodsService goodsService;
    private ObjectMapper objectMapper;
    private final static Logger LOGGER = Logger.getLogger(UserController.class);

    public GoodsController(GoodsService goodsService, ObjectMapper objectMapper) {
        this.goodsService = goodsService;
        this.objectMapper = objectMapper;
    }

    @GetMapping("/all")
    public ResponseEntity<String> getAllGoods() {
        try {
            Set<WebGood> goodsSet = goodsService.findAll();
            return new ResponseEntity<>(objectMapper.writeValueAsString(goodsSet), HttpStatus.OK);
        } catch (JsonProcessingException e) {
            return new ResponseEntity<>("Json mapper exception", HttpStatus.INTERNAL_SERVER_ERROR);
        } catch (DataAccessException e) {
            return new ResponseEntity<>("DataBase exception", HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PostMapping("/new")
    public ResponseEntity<String> newGood(@RequestBody WebGood webGood){
        try {
            WebGood good = goodsService.newGood(webGood);
            return new ResponseEntity<>(objectMapper.writeValueAsString(good), HttpStatus.OK);
        } catch (JsonProcessingException e) {
            return new ResponseEntity<>("Json mapper exception", HttpStatus.INTERNAL_SERVER_ERROR);
        } catch (DataAccessException e) {
            return new ResponseEntity<>("DataBase exception", HttpStatus.BAD_REQUEST);
        }
    }

    @PutMapping("/update")
    public ResponseEntity<String> goodUpdate(@RequestBody WebGood webGood){
        try {
            WebGood good = goodsService.update(webGood);
            return new ResponseEntity<>(objectMapper.writeValueAsString(good), HttpStatus.OK);
        } catch (JsonProcessingException e) {
            return new ResponseEntity<>("Json mapper exception", HttpStatus.INTERNAL_SERVER_ERROR);
        } catch (DataAccessException e) {
            return new ResponseEntity<>("DataBase exception", HttpStatus.BAD_REQUEST);
        } catch (IllegalArgumentException e) {
            return new ResponseEntity<>(e.getMessage(), HttpStatus.BAD_REQUEST);
        }
    }

    @DeleteMapping("/delete")
    public ResponseEntity<String> goodDelete(@RequestParam int id){
        try {
            goodsService.deleteById((long)id);
            return new ResponseEntity<>("", HttpStatus.OK);
        } catch (DataAccessException e) {
            return new ResponseEntity<>("DataBase exception", HttpStatus.INTERNAL_SERVER_ERROR);
        } catch (IllegalArgumentException e) {
            return new ResponseEntity<>(e.getMessage(), HttpStatus.BAD_REQUEST);
        }
    }

    @GetMapping("/details")
    public ResponseEntity<String> goodDetailsGet(@RequestParam int id){
        try {
            WebGood good = goodsService.findByIdC((long)id);
            return new ResponseEntity<>(objectMapper.writeValueAsString(good), HttpStatus.OK);
        } catch (JsonProcessingException e) {
            return new ResponseEntity<>("Json mapper exception", HttpStatus.INTERNAL_SERVER_ERROR);
        } catch (DataAccessException e) {
            return new ResponseEntity<>("DataBase exception", HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
}
