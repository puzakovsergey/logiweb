package com.javaschool.controller;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.javaschool.entity.driver.DriverStatus;
import com.javaschool.entity.geo.City;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

@RestController
@RequestMapping(path = "/api/service", produces = MediaType.APPLICATION_JSON_VALUE)
public class ServiceController {
    ObjectMapper objectMapper;

    public ServiceController(ObjectMapper objectMapper) {
        this.objectMapper = objectMapper;
    }

    @GetMapping("/get_Cities")
    public ResponseEntity<String> getCities() {
        try {
            List<String> cities = new ArrayList<>();
            for (City c : City.values()) {
                cities.add(c.name());
            }
            Collections.sort(cities);
            return new ResponseEntity<>(objectMapper.writeValueAsString(cities), HttpStatus.OK);
        } catch (JsonProcessingException e) {
            return new ResponseEntity<>("Json mapper exception", HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @GetMapping("/get_driver_statuses")
    public ResponseEntity<String> getDriverStatuses() {
        try {
            List<String> statuses = new ArrayList<>();
            for (DriverStatus ds : DriverStatus.values()) {
                statuses.add(ds.name());
            }
            Collections.sort(statuses);
            return new ResponseEntity<>(objectMapper.writeValueAsString(statuses), HttpStatus.OK);
        } catch (JsonProcessingException e) {
            return new ResponseEntity<>("Json mapper exception", HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
}
