package com.javaschool.controller;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.javaschool.model.WebOrder;
import com.javaschool.model.WebTruck;
import com.javaschool.service.OrderService;
import com.javaschool.service.TruckService;
import com.javaschool.service.utils.TruckConverter;
import org.apache.log4j.Logger;
import org.springframework.dao.DataAccessException;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.Set;

@RestController
@RequestMapping(path = "/api/trucks", produces = MediaType.APPLICATION_JSON_VALUE)
public class TruckController {
    private TruckService truckService;
    private ObjectMapper objectMapper;
    private OrderService orderService;
    private final static Logger LOGGER = Logger.getLogger(UserController.class);

    public TruckController(TruckService truckService, ObjectMapper objectMapper, OrderService orderService) {
        this.truckService = truckService;
        this.objectMapper = objectMapper;
        this.orderService = orderService;
    }

    @GetMapping("/all")
    public ResponseEntity<String> getAllTrucks() {
        try {
            Set<WebTruck> truckSet = truckService.findAll();
            return new ResponseEntity<>(objectMapper.writeValueAsString(truckSet), HttpStatus.OK);
        } catch (JsonProcessingException e) {
            return new ResponseEntity<>("Json mapper exception", HttpStatus.INTERNAL_SERVER_ERROR);
        } catch (DataAccessException e) {
            return new ResponseEntity<>("DataBase exception", HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PostMapping("/new")
    public ResponseEntity<String> newTruck(@RequestBody WebTruck webTruck){
        try {
            WebTruck truck = truckService.newTruck(webTruck);
            return new ResponseEntity<>(objectMapper.writeValueAsString(truck), HttpStatus.OK);
        } catch (JsonProcessingException e) {
            return new ResponseEntity<>("Json mapper exception", HttpStatus.INTERNAL_SERVER_ERROR);
        } catch (DataAccessException e) {
            return new ResponseEntity<>("DataBase exception", HttpStatus.INTERNAL_SERVER_ERROR);
        } catch (IllegalArgumentException e) {
            return new ResponseEntity<>(e.getMessage(), HttpStatus.BAD_REQUEST);
        } catch (Exception e) {
            return new ResponseEntity<>(e.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PutMapping("/update")
    public ResponseEntity<String> truckUpdate(@RequestBody WebTruck webTruck){
        try {
            WebTruck truck = truckService.update(webTruck);
            return new ResponseEntity<>(objectMapper.writeValueAsString(truck), HttpStatus.OK);
        } catch (JsonProcessingException e) {
            return new ResponseEntity<>("Json mapper exception", HttpStatus.INTERNAL_SERVER_ERROR);
        } catch (DataAccessException e) {
            return new ResponseEntity<>("DataBase exception", HttpStatus.INTERNAL_SERVER_ERROR);
        } catch (IllegalArgumentException e) {
            return new ResponseEntity<>(e.getMessage(), HttpStatus.BAD_REQUEST);
        } catch (Exception e) {
            return new ResponseEntity<>(e.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @DeleteMapping("/delete")
    public ResponseEntity<String> truckDelete(@RequestParam int id){
        try {
            truckService.deleteById((long)id);
            return new ResponseEntity<>("", HttpStatus.OK);
        } catch (DataAccessException e) {
            return new ResponseEntity<>("DataBase exception", HttpStatus.NOT_FOUND);
        } catch (IllegalArgumentException e) {
            return new ResponseEntity<>(e.getMessage(), HttpStatus.BAD_REQUEST);
        }
    }

    @GetMapping("/details")
    public ResponseEntity<String> truckDetails(@RequestParam int id){
        try {
            WebTruck truck = truckService.findByIdC((long)id);
            return new ResponseEntity<>(objectMapper.writeValueAsString(truck), HttpStatus.OK);
        } catch (JsonProcessingException e) {
            return new ResponseEntity<>("Json mapper exception", HttpStatus.INTERNAL_SERVER_ERROR);
        } catch (DataAccessException e) {
            return new ResponseEntity<>("DataBase exception", HttpStatus.NOT_FOUND);
        }
    }

    @GetMapping("/find_by_licenceplate")
    public ResponseEntity<String> truckFindByLicencePlate(@RequestParam String licencePlate) {
        try {
            Set<WebTruck> trucks = truckService.findByLicencePlate(licencePlate);
            return new ResponseEntity<>(objectMapper.writeValueAsString(trucks), HttpStatus.OK);
        } catch (JsonProcessingException e) {
            return new ResponseEntity<>("Json mapper exception", HttpStatus.INTERNAL_SERVER_ERROR);
        } catch (DataAccessException e) {
            return new ResponseEntity<>("DataBase exception", HttpStatus.NOT_FOUND);
        }
    }

    @GetMapping("/get_trucks_for_order")
    public ResponseEntity<String> getTrucksForOrder(@RequestBody WebOrder order){
        try {
            Set<WebTruck> trucks = orderService.getTrucksForOrder(order);
            return new ResponseEntity<>(objectMapper.writeValueAsString(trucks), HttpStatus.OK);
        } catch (JsonProcessingException e) {
            return new ResponseEntity<>("Json mapper exception", HttpStatus.INTERNAL_SERVER_ERROR);
        } catch (DataAccessException e) {
            return new ResponseEntity<>("DataBase exception", HttpStatus.NOT_FOUND);
        } catch (IllegalArgumentException e) {
            return new ResponseEntity<>(e.getMessage(), HttpStatus.BAD_REQUEST);
        } catch (Exception e) {
            return new ResponseEntity<>(e.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
}
