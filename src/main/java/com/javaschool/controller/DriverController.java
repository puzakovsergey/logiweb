package com.javaschool.controller;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.javaschool.entity.driver.DriverStatus;
import com.javaschool.model.WebDriver;
import com.javaschool.model.WebOrder;
import com.javaschool.service.DriverService;
import com.javaschool.service.OrderService;
import com.javaschool.service.utils.DriverConverter;
import org.apache.log4j.Logger;
import org.springframework.dao.DataAccessException;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.Set;

@RestController
@RequestMapping(path = "/api/drivers", produces = MediaType.APPLICATION_JSON_VALUE)
public class DriverController {
    private DriverService driverService;
    private ObjectMapper objectMapper;
    private OrderService orderService;
    private final static Logger LOGGER = Logger.getLogger(UserController.class);

    public DriverController(DriverService driverService, ObjectMapper objectMapper, OrderService orderService) {
        this.driverService = driverService;
        this.objectMapper = objectMapper;
        this.orderService = orderService;
    }

    @GetMapping("/all")
    public ResponseEntity<String> getAllDrivers() {
        try {
            Set<WebDriver> driverSet = driverService.findAll();
            return new ResponseEntity<>(objectMapper.writeValueAsString(driverSet), HttpStatus.OK);
        } catch (JsonProcessingException e) {
            return new ResponseEntity<>("Json mapper exception", HttpStatus.INTERNAL_SERVER_ERROR);
        } catch (DataAccessException e) {
            return new ResponseEntity<>("DataBase exception", HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PostMapping("/new")
    public ResponseEntity<String> newDriver(@RequestBody WebDriver webDriver){
        try {
            WebDriver driver = driverService.newDriver(webDriver);
            return new ResponseEntity<>(objectMapper.writeValueAsString(driver), HttpStatus.OK);
        } catch (JsonProcessingException e) {
            return new ResponseEntity<>("Json mapper exception", HttpStatus.INTERNAL_SERVER_ERROR);
        } catch (DataAccessException e) {
            return new ResponseEntity<>("DataBase exception", HttpStatus.INTERNAL_SERVER_ERROR);
        } catch (IllegalArgumentException e) {
            return new ResponseEntity<>(e.getMessage(), HttpStatus.BAD_REQUEST);
        } catch (Exception e) {
            return new ResponseEntity<>(e.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PutMapping("/update")
    public ResponseEntity<String> driverUpdate(@RequestBody WebDriver webDriver){
        try {
            WebDriver driver = driverService.update(webDriver);
            return new ResponseEntity<>(objectMapper.writeValueAsString(driver), HttpStatus.OK);
            } catch (JsonProcessingException e) {
                return new ResponseEntity<>("Json mapper exception", HttpStatus.INTERNAL_SERVER_ERROR);
            } catch (DataAccessException e) {
                return new ResponseEntity<>("DataBase exception", HttpStatus.INTERNAL_SERVER_ERROR);
            } catch (IllegalArgumentException e) {
                return new ResponseEntity<>(e.getMessage(), HttpStatus.BAD_REQUEST);
            } catch (Exception e) {
                return new ResponseEntity<>(e.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
            }
    }

    @DeleteMapping("/delete")
    public ResponseEntity<String> driverDelete(@RequestParam int id){
        try {
            driverService.deleteById((long)id);
            return new ResponseEntity<>("", HttpStatus.OK);
        } catch (DataAccessException e) {
            return new ResponseEntity<>("DataBase exception", HttpStatus.NOT_FOUND);
        } catch (IllegalArgumentException e) {
            return new ResponseEntity<>(e.getMessage(), HttpStatus.BAD_REQUEST);
        }
    }

    @GetMapping("/details")
    public ResponseEntity<String> driverDetails(@RequestParam int id){
        try {
            WebDriver driver = driverService.findByIdC((long)id);
            return new ResponseEntity<>(objectMapper.writeValueAsString(driver), HttpStatus.OK);
        } catch (JsonProcessingException e) {
            return new ResponseEntity<>("Json mapper exception", HttpStatus.INTERNAL_SERVER_ERROR);
        } catch (DataAccessException e) {
            return new ResponseEntity<>("DataBase exception", HttpStatus.BAD_REQUEST);
        } catch (IllegalArgumentException e) {
            return new ResponseEntity<>(e.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @GetMapping("/find_by_name")
    public ResponseEntity<String> driverFindByName(@RequestParam String name) {
        try {
            Set<WebDriver> drivers = driverService.findByName(name);
            return new ResponseEntity<>(objectMapper.writeValueAsString(drivers), HttpStatus.OK);
        } catch (JsonProcessingException e) {
            return new ResponseEntity<>("Json mapper exception", HttpStatus.INTERNAL_SERVER_ERROR);
        } catch (DataAccessException e) {
            return new ResponseEntity<>("DataBase exception", HttpStatus.BAD_REQUEST);
        }
    }

    @GetMapping("/free_drivers")
    public ResponseEntity<String> freeDrivers() {
        try {
            Set<WebDriver> drivers = driverService.findByStatus(DriverStatus.ON_REST);
            return new ResponseEntity<>(objectMapper.writeValueAsString(drivers), HttpStatus.OK);
        } catch (JsonProcessingException e) {
            return new ResponseEntity<>("Json mapper exception", HttpStatus.INTERNAL_SERVER_ERROR);
        } catch (DataAccessException e) {
            return new ResponseEntity<>("DataBase exception", HttpStatus.BAD_REQUEST);
        }
    }

    @PutMapping("/get_drivers_for_order")
    public ResponseEntity<String> getDriversForOrder(@RequestBody WebOrder order) {
        try {
            Set<WebDriver> drivers = orderService.getDriversSetForOrder(order);
            return new ResponseEntity<>(objectMapper.writeValueAsString(drivers), HttpStatus.OK);
        } catch (JsonProcessingException e) {
            return new ResponseEntity<>("Json mapper exception", HttpStatus.INTERNAL_SERVER_ERROR);
        } catch (DataAccessException e) {
            return new ResponseEntity<>("DataBase exception", HttpStatus.BAD_REQUEST);
        }
    }

}
