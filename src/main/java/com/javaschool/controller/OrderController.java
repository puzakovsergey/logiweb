package com.javaschool.controller;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.javaschool.model.WebDriver;
import com.javaschool.model.WebOrder;
import com.javaschool.model.WebTruck;
import com.javaschool.service.GoodsService;
import com.javaschool.service.OrderService;
import com.javaschool.service.utils.OrderConverter;
import org.springframework.dao.DataAccessException;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.Set;

@RestController
@RequestMapping(path = "/api/orders", produces = MediaType.APPLICATION_JSON_VALUE)
public class OrderController {
    private OrderService orderService;
    private GoodsService goodsService;
    private ObjectMapper objectMapper;

    public OrderController(OrderService orderService, GoodsService goodsService, ObjectMapper objectMapper) {
        this.orderService = orderService;
        this.goodsService = goodsService;
        this.objectMapper = objectMapper;
    }

    @PutMapping("/set_goods")
    public ResponseEntity<String> addGoodById(@RequestBody WebOrder order) {
        try {
            order = orderService.setGoods(order);
            return new ResponseEntity<>(objectMapper.writeValueAsString(order), HttpStatus.OK);
        } catch (DataAccessException e) {
            return new ResponseEntity<>("DataBase exception", HttpStatus.BAD_REQUEST);
        } catch (JsonProcessingException e) {
            return new ResponseEntity<>("Json mapper exception", HttpStatus.INTERNAL_SERVER_ERROR);
        } catch (IllegalArgumentException e) {
            return new ResponseEntity<>(e.getMessage(), HttpStatus.BAD_REQUEST);
        }
    }
    @GetMapping("/all")
    public ResponseEntity<String> getAllOrders() {
        try {
            Set<WebOrder> ordersSet = orderService.findAll();
            return new ResponseEntity<>(objectMapper.writeValueAsString(ordersSet), HttpStatus.OK);
        } catch (JsonProcessingException e) {
            return new ResponseEntity<>("Json mapper exception", HttpStatus.INTERNAL_SERVER_ERROR);
        } catch (DataAccessException e) {
            return new ResponseEntity<>("DataBase exception", HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @GetMapping("/details")
    public ResponseEntity<String> details(@RequestParam int id) {
        try {
            WebOrder order = orderService.findById((long)id);
            return new ResponseEntity<>(objectMapper.writeValueAsString(order), HttpStatus.OK);
        } catch (JsonProcessingException e) {
            return new ResponseEntity<>("Json mapper exception", HttpStatus.INTERNAL_SERVER_ERROR);
        } catch (DataAccessException e) {
            return new ResponseEntity<>("DataBase exception", HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @DeleteMapping("/delete")
    public ResponseEntity<String> delete(@RequestParam int id) {
        try {
            orderService.deleteById((long)id);
            return new ResponseEntity<>("", HttpStatus.OK);
        } catch (DataAccessException e) {
        return new ResponseEntity<>("DataBase exception", HttpStatus.INTERNAL_SERVER_ERROR);
        } catch (IllegalArgumentException e) {
        return new ResponseEntity<>(e.getMessage(), HttpStatus.LOCKED);
        }
    }

    @GetMapping("/new")
    public ResponseEntity<String> newOrder() {
        try {
            WebOrder order = orderService.newOrder();
            return new ResponseEntity<>(objectMapper.writeValueAsString(order), HttpStatus.OK);
        } catch (JsonProcessingException e) {
            return new ResponseEntity<>("Json mapper exception", HttpStatus.INTERNAL_SERVER_ERROR);
        } catch (DataAccessException e) {
            return new ResponseEntity<>("DataBase exception", HttpStatus.BAD_REQUEST);
        }
    }

    @PutMapping("/set_driver")
    public ResponseEntity<String> setDriver(@RequestBody WebOrder order) {
        try {
            order = orderService.setDriver(order);
            return new ResponseEntity<>(objectMapper.writeValueAsString(order), HttpStatus.OK);
        } catch (JsonProcessingException e) {
            return new ResponseEntity<>("Json mapper exception", HttpStatus.INTERNAL_SERVER_ERROR);
        } catch (DataAccessException e) {
            return new ResponseEntity<>("DataBase exception", HttpStatus.BAD_REQUEST);
        } catch (IllegalArgumentException e) {
            return new ResponseEntity<>(e.getMessage(), HttpStatus.BAD_REQUEST);
        }
    }

    @PutMapping("/set_truck")
    public ResponseEntity<String> setTruck(@RequestBody WebOrder order) {
        try {
            order = orderService.setTruck(order);
            return new ResponseEntity<>(objectMapper.writeValueAsString(order), HttpStatus.OK);
        } catch (JsonProcessingException e) {
            return new ResponseEntity<>("Json mapper exception", HttpStatus.INTERNAL_SERVER_ERROR);
        } catch (DataAccessException e) {
            return new ResponseEntity<>("DataBase exception", HttpStatus.BAD_REQUEST);
        } catch (IllegalArgumentException e) {
            return new ResponseEntity<>(e.getMessage(), HttpStatus.BAD_REQUEST);
        }
    }

    @GetMapping("/get_orders_for_truck")
    public ResponseEntity<String> getOrdersForTruck(@RequestBody WebTruck truck) {
        try {
            Set<WebOrder> orders = orderService.findOrdersByTruck(truck);
            return new ResponseEntity<>(objectMapper.writeValueAsString(orders), HttpStatus.OK);
        } catch (JsonProcessingException e) {
            return new ResponseEntity<>("Json mapper exception", HttpStatus.INTERNAL_SERVER_ERROR);
        } catch (DataAccessException e) {
            return new ResponseEntity<>("DataBase exception", HttpStatus.BAD_REQUEST);
        } catch (IllegalArgumentException e) {
            return new ResponseEntity<>(e.getMessage(), HttpStatus.BAD_REQUEST);
        }
    }

    @GetMapping("/get_orders_for_driver")
    public ResponseEntity<String> getOrdersForDriver(@RequestBody WebDriver driver) {
        try {
            Set<WebOrder> orders = orderService.findOrdersByDriver(driver);
            return new ResponseEntity<>(objectMapper.writeValueAsString(orders), HttpStatus.OK);
        } catch (JsonProcessingException e) {
            return new ResponseEntity<>("Json mapper exception", HttpStatus.INTERNAL_SERVER_ERROR);
        } catch (DataAccessException e) {
            return new ResponseEntity<>("DataBase exception", HttpStatus.BAD_REQUEST);
        } catch (IllegalArgumentException e) {
            return new ResponseEntity<>(e.getMessage(), HttpStatus.BAD_REQUEST);
        }
    }
}
