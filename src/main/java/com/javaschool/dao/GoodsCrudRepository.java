package com.javaschool.dao;

import com.javaschool.entity.order.goods.GoodsEntity;
import com.javaschool.entity.user.UserEntity;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.Collection;
import java.util.Set;

@Repository
public interface GoodsCrudRepository extends CrudRepository<GoodsEntity, Long> {
    Set<GoodsEntity> findAll();
    Set<GoodsEntity> findAllByOrder_Id(Long id);
    Set<GoodsEntity> findAllByIdIn(Collection<Long> ids);
}
