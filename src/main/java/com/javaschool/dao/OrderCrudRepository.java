package com.javaschool.dao;

import com.javaschool.entity.driver.DriverEntity;
import com.javaschool.entity.geo.City;
import com.javaschool.entity.order.OrderEntity;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.Date;
import java.util.List;
import java.util.Set;

@Repository
public interface OrderCrudRepository extends CrudRepository<OrderEntity, Long> {
    List<OrderEntity> findAllByEndDateAfter(Date date);
    Set<OrderEntity> findAllByEndDateAfterAndDriver(Date date, DriverEntity driver);
    Set<OrderEntity> findAllByDriver(DriverEntity driver);
    Set<OrderEntity> findAll();
    Set<OrderEntity> findAllByMaxWeightIsLessThanEqualAndStartDateNull(int weight);
    Set<OrderEntity> findAllByTruck_CurrentCityAndTimeForDeliveryLessThanEqualAndStartDateNull(City city, int timeForDelivery);
}
