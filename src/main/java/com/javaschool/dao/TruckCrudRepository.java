package com.javaschool.dao;

import com.javaschool.entity.TruckEntity;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.Set;

@Repository
public interface TruckCrudRepository extends CrudRepository<TruckEntity, Long> {
    Set<TruckEntity> findAll();

    @Query("SELECT t FROM TruckEntity t WHERE t.licencePlate LIKE CONCAT('%', :licencePlate, '%')")
    Set<TruckEntity> findAllByLicencePlateContent(@Param("licencePlate") String licencePlate);

    @Query("SELECT t FROM TruckEntity t WHERE t.fixed=FALSE")
    Set<TruckEntity> findBroken();

    @Query("SELECT t FROM TruckEntity t WHERE t.fixed = TRUE AND t.capacity >= :capacity")
    Set<TruckEntity> findReadyForOrder(@Param("capacity") int capacity);

}
