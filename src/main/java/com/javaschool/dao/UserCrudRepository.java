package com.javaschool.dao;

import com.javaschool.entity.user.UserEntity;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;
import java.util.Set;

@Repository
public interface UserCrudRepository extends CrudRepository<UserEntity, Long> {
    UserEntity findUserEntityByLogin(String login);
    Set<UserEntity> findAll();
}
