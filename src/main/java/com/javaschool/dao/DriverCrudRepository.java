package com.javaschool.dao;

import com.javaschool.entity.driver.DriverEntity;
import com.javaschool.entity.driver.DriverStatus;
import com.javaschool.entity.geo.City;
import com.javaschool.entity.order.OrderEntity;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.Optional;
import java.util.Set;

@Repository
public interface DriverCrudRepository extends CrudRepository<DriverEntity, Long> {
    Set<DriverEntity> findAllByCurrentCity(City city);
    Set<DriverEntity> findAllByLastName(String lastName);
    Set<DriverEntity> findAllByStatus(DriverStatus status);
    Set<DriverEntity> findAllByStatusAndCurrentCity(DriverStatus status, City city);
    Set<DriverEntity> findAll();

    @Query("SELECT d FROM DriverEntity d WHERE d.firstName LIKE CONCAT('%', :name, '%') OR d.lastName LIKE CONCAT('%', :name, '%')")
    Set<DriverEntity> findByName(@Param("name") String name);
}
