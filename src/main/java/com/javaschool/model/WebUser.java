package com.javaschool.model;

import com.javaschool.entity.user.UserRole;

import java.util.Objects;

public class WebUser {
    private Long id;
    private String login = "";
    private String password = "";
    private UserRole role;

    public WebUser() {
    }

    public WebUser(String login, String password, UserRole role) {
        setLogin(login);
        setPassword(password);
        setRole(role);
    }

    public WebUser(Long id, String login, UserRole role) {
        this.id = id;
        this.login = login;
        this.role = role;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public UserRole getRole() {
        return role;
    }

    public void setRole(UserRole role) {
        this.role = role;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof WebUser)) return false;
        WebUser webUser = (WebUser) o;
        return id == webUser.id &&
                Objects.equals(login, webUser.login) &&
                Objects.equals(password, webUser.password) &&
                role == webUser.role;
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, login, password, role);
    }

    @Override
    public String toString() {
        return "WebUser{" +
                "id=" + id +
                ", login='" + login + '\'' +
                ", password='" + password + '\'' +
                ", role=" + role +
                '}';
    }
}
