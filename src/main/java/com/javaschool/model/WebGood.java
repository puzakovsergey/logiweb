package com.javaschool.model;

import com.javaschool.entity.geo.City;

import java.time.LocalDateTime;
import java.util.Date;

public class WebGood {
    private Long id;

    private City cityForLoading;

    private City cityForUnloading;

    private String name;

    private int weight;

    private Long orderId;

    private Date dateReady;

    private Date dateLoaded;

    private Date dateUnloaded;

    private String notes;

    public Long getOrderId() {
        return orderId;
    }

    public void setOrderId(Long orderId) {
        this.orderId = orderId;
    }

    public Date getDateReady() {
        return dateReady;
    }

    public void setDateReady(Date dateReady) {
        this.dateReady = dateReady;
    }

    public Date getDateLoaded() {
        return dateLoaded;
    }

    public void setDateLoaded(Date dateLoaded) {
        this.dateLoaded = dateLoaded;
    }

    public Date getDateUnloaded() {
        return dateUnloaded;
    }

    public void setDateUnloaded(Date dateUnloaded) {
        this.dateUnloaded = dateUnloaded;
    }

    public String getNotes() {
        return notes;
    }

    public void setNotes(String notes) {
        this.notes = notes;
    }

    public WebGood() {
    }

    public WebGood(Long id, City cityForLoading, City cityForUnloading, String name, int weight, Long orderId, Date dateReady, Date dateLoaded, Date dateUnloaded) {
        this.id = id;
        this.cityForLoading = cityForLoading;
        this.cityForUnloading = cityForUnloading;
        this.name = name;
        this.weight = weight;
        this.orderId = orderId;
        this.dateReady = dateReady;
        this.dateLoaded = dateLoaded;
        this.dateUnloaded = dateUnloaded;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public City getCityForLoading() {
        return cityForLoading;
    }

    public void setCityForLoading(City cityForLoading) {
        this.cityForLoading = cityForLoading;
    }

    public City getCityForUnloading() {
        return cityForUnloading;
    }

    public void setCityForUnloading(City cityForUnloading) {
        this.cityForUnloading = cityForUnloading;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getWeight() {
        return weight;
    }

    public void setWeight(int weight) {
        this.weight = weight;
    }
}
