package com.javaschool.model;

import com.javaschool.entity.geo.City;
import com.javaschool.entity.order.WaypointsType;

public class WebWaypoint {
    private Long id;

    private Long goodId;

    private int number;

    private City city;

    private WaypointsType type;

    private Long orderId;

    private boolean reached = false;

    public WebWaypoint() {
    }

    public boolean isReached() {
        return reached;
    }

    public void setReached(boolean reached) {
        this.reached = reached;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getGoodId() {
        return goodId;
    }

    public void setGoodId(Long goodId) {
        this.goodId = goodId;
    }

    public int getNumber() {
        return number;
    }

    public void setNumber(int number) {
        this.number = number;
    }

    public City getCity() {
        return city;
    }

    public void setCity(City city) {
        this.city = city;
    }

    public WaypointsType getType() {
        return type;
    }

    public void setType(WaypointsType type) {
        this.type = type;
    }

    public Long getOrderId() {
        return orderId;
    }

    public void setOrderId(Long orderId) {
        this.orderId = orderId;
    }
}
