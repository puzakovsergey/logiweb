package com.javaschool.entity.driver;

public enum DriverStatus {
    ON_REST,
    ON_DRIVE
}
