package com.javaschool.entity.order.goods;

public enum GoodsStatus {
    READY,
    SHIPPED,
    DELIVERED
}
