package com.javaschool.entity;

import com.javaschool.entity.geo.City;
import com.javaschool.entity.order.OrderEntity;

import javax.persistence.*;
import java.util.Objects;

@Entity
@Table(name = "Trucks")
public class TruckEntity {
    @Id
    @Column(name = "ID")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "Licence_Plate", nullable = false, unique = true)
    private String licencePlate;

    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "Order_ID")
    private OrderEntity order;

    @Column(name = "Current_City", nullable = false)
    @Enumerated(EnumType.STRING)
    private City currentCity;

    @Column(name = "Fixed", nullable = false)
    private Boolean fixed;

    @Column(name = "Capacity", nullable = false)
    private Integer capacity;

    @Column(name = "Notes")
    private String notes = "";

    public TruckEntity() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getLicencePlate() {
        return licencePlate;
    }

    public void setLicencePlate(String licencePlate) {
        this.licencePlate = licencePlate;
    }

    public OrderEntity getOrder() {
        return order;
    }

    public void setOrder(OrderEntity order) {
        this.order = order;
    }

    public City getCurrentCity() {
        return currentCity;
    }

    public void setCurrentCity(City currentCity) {
        this.currentCity = currentCity;
    }

    public Boolean getFixed() {
        return fixed;
    }

    public void setFixed(Boolean fixed) {
        this.fixed = fixed;
    }

    public Integer getCapacity() {
        return capacity;
    }

    public void setCapacity(Integer capacity) {
        this.capacity = capacity;
    }

    public String getNotes() {
        return notes;
    }

    public void setNotes(String notes) {
        this.notes = notes;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof TruckEntity)) return false;
        TruckEntity that = (TruckEntity) o;
        return id.equals(that.id) &&
                licencePlate.equals(that.licencePlate) &&
                Objects.equals(order, that.order) &&
                currentCity == that.currentCity &&
                fixed.equals(that.fixed) &&
                capacity.equals(that.capacity) &&
                Objects.equals(notes, that.notes);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, licencePlate, order, currentCity, fixed, capacity, notes);
    }
}
