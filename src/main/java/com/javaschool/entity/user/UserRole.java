package com.javaschool.entity.user;

public enum UserRole {
    ADMIN,
    USER,
    DRIVER
}
