package com.javaschool.entity.geo;

public enum City {
    VORONEZH,
    MOSCOW,
    ROSTOV,
    NOVGOROD
}
