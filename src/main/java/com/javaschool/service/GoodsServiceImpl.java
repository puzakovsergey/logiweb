package com.javaschool.service;

import com.javaschool.dao.GoodsCrudRepository;
import com.javaschool.entity.geo.City;
import com.javaschool.entity.order.OrderEntity;
import com.javaschool.entity.order.goods.GoodsEntity;
import com.javaschool.model.WebGood;
import com.javaschool.service.utils.GoodConverter;
import com.javaschool.service.utils.Utils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Collection;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;

/**
 * This class contains methods for working with goods
 */
@Service
@EnableJpaRepositories(basePackages = {"com.javaschool.dao"})
public class GoodsServiceImpl implements GoodsService {

    GoodsCrudRepository goodsCrudRepository;

    public GoodsServiceImpl(GoodsCrudRepository goodsCrudRepository) {
        this.goodsCrudRepository = goodsCrudRepository;
    }

    @Override
    @Transactional
    public Set<WebGood> findAll() throws DataAccessException {
        Set<GoodsEntity> goods = goodsCrudRepository.findAll();
        return GoodConverter.goodsToWebGoods(goods);
    }

    @Override
    @Transactional
    public GoodsEntity findById(Long id) throws DataAccessException, IllegalArgumentException {
        GoodsEntity good = goodsCrudRepository.findById(id).orElse(new GoodsEntity());
        if (good.getId() == null) {
            throw new IllegalArgumentException("Not found");
        }
        return good;
    }

    @Override
    @Transactional
    public WebGood findByIdC(Long id) throws DataAccessException, IllegalArgumentException {
        WebGood good = GoodConverter.goodToWebGood(goodsCrudRepository.findById(id).orElse(new GoodsEntity()));
        if (good.getId() == null) {
            throw new IllegalArgumentException("Not found");
        }
        return good;
    }

    @Override
    @Transactional
    public WebGood newGood(WebGood webGood) throws DataAccessException {
        GoodsEntity good = GoodConverter.webGoodToGood(webGood);
        good = goodsCrudRepository.save(good);
        return GoodConverter.goodToWebGood(good);
    }

    /**
     * deleteById method. Removes GoodEntity from DB using a good identifier.
     * @param id - good identifier
     * @throws DataAccessException
     * @throws IllegalArgumentException
     */
    @Override
    @Transactional
    public void deleteById(Long id) throws DataAccessException, IllegalArgumentException {
        GoodsEntity good = findById(id);
        if (good.getDateReady()==null) {
            goodsCrudRepository.deleteById(id);
        } else {
            throw new IllegalArgumentException("Can't delete Good. Good is in delivery");
        }
    }

    @Override
    @Transactional
    public WebGood update(WebGood good) throws DataAccessException, IllegalArgumentException {
        if (good == null) {
            throw new IllegalArgumentException("Wrong parameters");
        }
        if (good.getId() == null) {
            throw new IllegalArgumentException("Wrong parameters");
        }
        GoodsEntity goodsEntity = findById(good.getId());
        if (goodsEntity.getDateReady() != null) {
            throw new IllegalArgumentException("Can't update good. Good is on delivery.");
        }
        if (good.getWeight() > 0) {
            goodsEntity.setWeight(good.getWeight());
        }
        if (good.getCityForUnloading() != null) {
            goodsEntity.setCityForUnloading(good.getCityForUnloading());
        }
        if (good.getCityForLoading() != null) {
            goodsEntity.setCityForLoading(good.getCityForLoading());
        }
        if (good.getNotes() != null) {
            goodsEntity.setNotes(good.getNotes());
        }
        if (good.getName() != null) {
            goodsEntity.setName(good.getName());
        }
        goodsEntity = goodsCrudRepository.save(goodsEntity);

        return GoodConverter.goodToWebGood(goodsEntity);
    }

    @Override
    @Transactional
    public Set<GoodsEntity> getGoodsByIds(Collection<Long> ids) throws DataAccessException {
        return goodsCrudRepository.findAllByIdIn(ids);
    }

    @Override
    @Transactional
    public Set<GoodsEntity> saveGoods(Set<GoodsEntity> goods) throws DataAccessException {
        Set<GoodsEntity> savedGoods = new HashSet<>();
        for (GoodsEntity g : goods) {
            savedGoods.add(goodsCrudRepository.save(g));
        }
        return savedGoods;
    }

    @Override
    @Transactional
    public GoodsEntity saveGood(GoodsEntity good) throws DataAccessException {
        return goodsCrudRepository.save(good);
    }

}
