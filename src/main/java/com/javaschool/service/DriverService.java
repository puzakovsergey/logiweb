package com.javaschool.service;

import com.javaschool.entity.driver.DriverEntity;
import com.javaschool.entity.driver.DriverStatus;
import com.javaschool.entity.geo.City;
import com.javaschool.entity.order.OrderEntity;
import com.javaschool.entity.user.UserEntity;
import com.javaschool.model.WebDriver;
import com.javaschool.model.WebOrder;
import com.javaschool.model.WebUser;
import org.springframework.dao.DataAccessException;

import java.util.List;
import java.util.Set;

public interface DriverService {
    Set<WebDriver> findAll() throws DataAccessException;
    Set<WebDriver> findByName(String lastName) throws DataAccessException;
    Set<WebDriver> findByStatus(DriverStatus status) throws DataAccessException;
    DriverEntity findById(Long id) throws DataAccessException;
    WebDriver findByIdC(Long id) throws DataAccessException;
    WebDriver newDriver(WebDriver driver) throws DataAccessException, IllegalArgumentException;
    void deleteById(Long id) throws DataAccessException;
    WebDriver update(WebDriver driver) throws DataAccessException, IllegalArgumentException;
    Set<DriverEntity> getFreeDriversByCity (City city);
    DriverEntity saveDriver(DriverEntity driver) throws DataAccessException;
}
