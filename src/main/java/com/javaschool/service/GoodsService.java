package com.javaschool.service;

import com.javaschool.entity.geo.City;
import com.javaschool.entity.order.goods.GoodsEntity;
import com.javaschool.model.WebGood;
import org.springframework.dao.DataAccessException;

import java.util.Collection;
import java.util.Set;

public interface GoodsService {
    Set<WebGood> findAll() throws DataAccessException;
    GoodsEntity findById(Long id) throws DataAccessException;
    WebGood findByIdC(Long id) throws DataAccessException;
    WebGood newGood(WebGood webGood) throws DataAccessException;
    void deleteById(Long id) throws DataAccessException;
    WebGood update(WebGood good) throws DataAccessException, IllegalArgumentException;
    Set<GoodsEntity> getGoodsByIds(Collection<Long> ids) throws DataAccessException;
    Set<GoodsEntity> saveGoods(Set<GoodsEntity> goods) throws DataAccessException;
    GoodsEntity saveGood(GoodsEntity good) throws DataAccessException;
}
