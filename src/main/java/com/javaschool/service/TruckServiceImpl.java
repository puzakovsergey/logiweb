package com.javaschool.service;

import com.javaschool.dao.TruckCrudRepository;
import com.javaschool.entity.TruckEntity;
import com.javaschool.model.WebTruck;
import com.javaschool.service.utils.TruckConverter;
import org.springframework.dao.DataAccessException;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.HashSet;
import java.util.Set;

@Service
@EnableJpaRepositories(basePackages = {"com.javaschool.dao"})
public class TruckServiceImpl implements TruckService {
    private TruckCrudRepository truckCrudRepository;

    public TruckServiceImpl(TruckCrudRepository truckCrudRepository) {
        this.truckCrudRepository = truckCrudRepository;
    }

    @Override
    @Transactional
    public Set<WebTruck> findAll() throws DataAccessException {
        Set<TruckEntity> trucks = truckCrudRepository.findAll();
        return TruckConverter.trucksToWebTrucks(trucks);
    }

    @Override
    @Transactional
    public Set<WebTruck> findByLicencePlate(String licencePlate) throws DataAccessException {
        if (licencePlate == null || licencePlate.equals("")) {
            return new HashSet<>();
        }
        Set<TruckEntity> trucks = truckCrudRepository.findAllByLicencePlateContent(licencePlate);
        return TruckConverter.trucksToWebTrucks(trucks);
    }

    @Override
    @Transactional
    public Set<WebTruck> findBroken() throws DataAccessException {
        Set<TruckEntity> trucks = truckCrudRepository.findBroken();
        return TruckConverter.trucksToWebTrucks(trucks);
    }

    @Override
    @Transactional
    public TruckEntity findById(Long id) throws DataAccessException {
        TruckEntity truck = truckCrudRepository.findById(id).orElse(new TruckEntity());
        if (truck.getId() == null) {
            throw new IllegalArgumentException("Not found");
        }
        return truck;
    }

    @Override
    @Transactional
    public WebTruck findByIdC(Long id) throws DataAccessException {
        WebTruck truck = TruckConverter.truckToWebTruck(truckCrudRepository.findById(id).orElse(new TruckEntity()));
        if (truck.getId() == null) {
            throw new IllegalArgumentException("Not found");
        }
        return truck;
    }

    @Override
    @Transactional
    public WebTruck newTruck(WebTruck truck) throws DataAccessException, IllegalArgumentException {
        if (truck == null) {
            throw new IllegalArgumentException("Truck is NULL");
        }
        TruckEntity truckEntity = TruckConverter.webTruckToTruck(truck);
        truckEntity = truckCrudRepository.save(truckEntity);
        return TruckConverter.truckToWebTruck(truckEntity);
    }

    @Override
    @Transactional
    public void deleteById(Long id) throws DataAccessException, IllegalArgumentException {
        TruckEntity truck = findById(id);
        if (truck.getOrder() == null) {
            truckCrudRepository.deleteById(id);
        } else {
            throw new IllegalArgumentException("Can't delete truck. Truck is on duty");
        }
    }

    @Override
    @Transactional
    public WebTruck update(WebTruck truck) throws DataAccessException, IllegalArgumentException {
        if (truck == null) {
            throw new IllegalArgumentException("Wrong parameters");
        }
        if (truck.getId() == null) {
            throw new IllegalArgumentException("Wrong parameters");
        }
        TruckEntity truckEntity = findById(truck.getId());
        if (truckEntity.getOrder() == null) {
            if (truck.getCurrentCity() != null) {
                truckEntity.setCurrentCity(truck.getCurrentCity());
            }
            if (truck.getCapacity() > 0) {
                truckEntity.setCapacity(truck.getCapacity());
            }
            if (truck.getLicencePlate() != null && !truck.getLicencePlate().equals("")) {
                truckEntity.setLicencePlate(truck.getLicencePlate());
            }
            if (truck.getNotes() != null) {
                truckEntity.setNotes(truck.getNotes());
            }
            truckEntity.setFixed(truck.isFixed());
            truckEntity = truckCrudRepository.save(truckEntity);
            WebTruck webTruck = TruckConverter.truckToWebTruck(truckEntity);
            return webTruck;
        } else {
            throw new IllegalArgumentException("Can't update truck. Truck is on duty");
        }
    }

    @Override
    @Transactional
    public Set<WebTruck> getReadyForOrderTrucks(int capacity) throws DataAccessException {
        Set<TruckEntity> trucks = truckCrudRepository.findReadyForOrder(capacity);
        return TruckConverter.trucksToWebTrucks(trucks);
    }

    @Override
    @Transactional
    public TruckEntity saveTruck(TruckEntity truck) throws DataAccessException{
        return truckCrudRepository.save(truck);
    }
}
