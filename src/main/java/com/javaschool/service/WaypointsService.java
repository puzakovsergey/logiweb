package com.javaschool.service;

import com.javaschool.entity.order.OrderEntity;
import com.javaschool.entity.order.WaypointsEntity;
import org.springframework.dao.DataAccessException;

import java.util.List;
import java.util.Set;

public interface WaypointsService {
    void deleteById(Long id) throws DataAccessException;
    Set<WaypointsEntity> getSetOfWaypointsByOrderId(Long id) throws DataAccessException;
    void newWaypoint(WaypointsEntity waypoint) throws DataAccessException;
    void deleteByIds(Set<Long> ids) throws DataAccessException, IllegalArgumentException;
    Set<WaypointsEntity> saveAll(Set<WaypointsEntity> waypoints) throws DataAccessException;
    WaypointsEntity findById(Long id) throws DataAccessException;
    WaypointsEntity saveWaypoint(WaypointsEntity waypoint) throws DataAccessException;
}
