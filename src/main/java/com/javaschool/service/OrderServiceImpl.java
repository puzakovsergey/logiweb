package com.javaschool.service;

import com.javaschool.dao.OrderCrudRepository;
import com.javaschool.entity.TruckEntity;
import com.javaschool.entity.driver.DriverEntity;
import com.javaschool.entity.driver.DriverStatus;
import com.javaschool.entity.order.OrderEntity;
import com.javaschool.entity.order.WaypointsEntity;
import com.javaschool.entity.order.WaypointsType;
import com.javaschool.entity.order.goods.GoodsEntity;
import com.javaschool.model.*;
import com.javaschool.service.utils.DriverConverter;
import com.javaschool.service.utils.OrderConverter;
import com.javaschool.service.utils.WaypointConverter;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;
import org.springframework.dao.DataAccessException;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.*;

/**
 *
 */
@Service
@PropertySource("classpath:logiweb.properties")
@EnableJpaRepositories(basePackages = {"com.javaschool.dao"})
public class    OrderServiceImpl implements OrderService {

    private OrderCrudRepository orderCrudRepository;
    private WaypointsService waypointsService;
    private DriverService driverService;
    private GoodsService goodsService;
    private TruckService truckService;

    @Value("${application.maxHoursInMonth:170}")
    private int maxHoursInMonth;

    private final static Logger LOGGER = Logger.getLogger(UserServiceImpl.class);

    public OrderServiceImpl(OrderCrudRepository orderCrudRepository, WaypointsService waypointsService, DriverService driverService, GoodsService goodsService, TruckService truckService) {
        this.orderCrudRepository = orderCrudRepository;
        this.waypointsService = waypointsService;
        this.driverService = driverService;
        this.goodsService = goodsService;
        this.truckService = truckService;
    }

    /**
     *
     * @return
     * @throws DataAccessException
     */
    @Override
    @Transactional
    public Set<WebOrder> findAll() throws DataAccessException {
        Set<OrderEntity> orders = orderCrudRepository.findAll();
        return OrderConverter.ordersToWebOrders(orders);
    }

    /**
     *
     * @param id
     * @return
     * @throws DataAccessException
     */
    @Override
    @Transactional
    public OrderEntity findOrderById(Long id) throws DataAccessException {
        OrderEntity order = orderCrudRepository.findById(id).get();
        return order;
    }

    @Override
    @Transactional
    public WebOrder findById(Long id) throws DataAccessException {
        OrderEntity order = orderCrudRepository.findById(id).get();
        return OrderConverter.orderToWebOrder(order);
    }


    @Override
    @Transactional
    public WebOrder newOrder() throws DataAccessException {
        OrderEntity order = new OrderEntity();
        order = orderCrudRepository.save(order);
        return OrderConverter.orderToWebOrder(order);
    }

    @Override
    @Transactional
    public void deleteById(Long id) throws DataAccessException, IllegalArgumentException {
        OrderEntity order = findOrderById(id);
        if (order.getStartDate() == null || order.getStartDate().getTime() < 1) {
            orderCrudRepository.deleteById(id);
        } else {
            throw new IllegalArgumentException("Can't delete order. Order is on delivery");
        }

    }

    @Override
    @Transactional
    public WebOrder setTruck(WebOrder order) throws DataAccessException, IllegalArgumentException {
        if (order == null || order.getId() == null) {
            throw new IllegalArgumentException("Wrong parameters");
        } else if (order.getTruck() == null || order.getTruck().getId() == null) {
            throw new IllegalArgumentException("Wrong parameters");
        }
        OrderEntity orderEntity = findOrderById(order.getId());
        TruckEntity truckEntity = truckService.findById(order.getTruck().getId());
        if (order.getStartDate() == null || order.getStartDate().getTime() < 1) {
            if (orderEntity.getMaxWeight() > 0 && truckEntity.getCapacity() >= orderEntity.getMaxWeight()) {
                truckEntity.setOrder(orderEntity);
                truckEntity = truckService.saveTruck(truckEntity);
                orderEntity.setTruck(truckEntity);
                orderEntity = orderCrudRepository.save(orderEntity);
                return OrderConverter.orderToWebOrder(orderEntity);
            } else {
                throw new IllegalArgumentException("Can't assign truck. Truck is to small, or order has no goods.");
            }
        } else {
            throw new IllegalArgumentException("Can't assign truck. Order is on delivery.");
        }
    }

    @Override
    @Transactional
    public WebOrder setDriver(WebOrder order) throws DataAccessException, IllegalArgumentException {
        if (order == null || order.getId() == null) {
            throw new IllegalArgumentException("Wrong parameters");
        } else if (order.getDriver() == null || order.getDriver().getId() == null) {
            throw new IllegalArgumentException("Wrong parameters");
        }
        OrderEntity orderEntity = findOrderById(order.getId());
        DriverEntity driverEntity = driverService.findById(order.getDriver().getId());
        if ((order.getStartDate() == null || order.getStartDate().getTime() < 1) && orderEntity.getTruck().getCurrentCity().equals(driverEntity.getCurrentCity())) {
            int expectedHoursOnDutyInMonth = getDriverHoursOnDutyInMonth(driverEntity) + orderEntity.getTimeForDelivery();
            if (orderEntity.getTimeForDelivery() > 0 && expectedHoursOnDutyInMonth <= maxHoursInMonth) {
                driverEntity.setOrder(orderEntity);
                driverEntity = driverService.saveDriver(driverEntity);
                orderEntity.setDriver(driverEntity);
                orderEntity = orderCrudRepository.save(orderEntity);
                return OrderConverter.orderToWebOrder(orderEntity);
            } else {
                throw new IllegalArgumentException("Can't assign driver. Driver has too many work hours in this month, or order has no goods");
            }
        } else {
            throw new IllegalArgumentException("Can't assign driver. Order is on delivery, or current city of truck and driver do not match");
        }
    }

    @Override
    @Transactional
    public int getDriverHoursOnDutyInMonth(DriverEntity driver) throws DataAccessException {
        int hoursOnDuty = 0;
        Set<OrderEntity> orders = findCompleteOrdersForLastDaysForDriver(driver);
        for (OrderEntity o : orders) {
            hoursOnDuty = hoursOnDuty + o.getEndDate().getHours()-o.getStartDate().getHours();
        }
        return hoursOnDuty;
    }

    @Override
    @Transactional
    public Set<WebDriver> getDriversSetForOrder(WebOrder webOrder) throws DataAccessException, IllegalArgumentException{
        if (webOrder == null || webOrder.getId() == null) {
            throw new IllegalArgumentException("Wrong parameters");
        }
        OrderEntity order = findOrderById(webOrder.getId());
        if (order.getTruck() == null) {
            return new HashSet<>();
        }
        Set<DriverEntity> drivers = driverService.getFreeDriversByCity(order.getTruck().getCurrentCity());
        Set<DriverEntity> driversSet = new HashSet<>();
        for (DriverEntity d : drivers) {
            if (getDriverHoursOnDutyInMonth(d) + order.getTimeForDelivery() <= maxHoursInMonth) {
                driversSet.add(d);
            }
        }
        return DriverConverter.driversToWebDrivers(driversSet);
    }

    @Override
    @Transactional
    public Set<OrderEntity> findCompleteOrdersForLastDaysForDriver (DriverEntity driver) throws DataAccessException {
        Calendar date = new GregorianCalendar();
        date.add(Calendar.DATE, -30);
        return orderCrudRepository.findAllByEndDateAfterAndDriver(date.getTime(), driver);
    }

    @Override
    @Transactional
    public WebOrder setGoods(WebOrder order) throws DataAccessException, IllegalArgumentException {
        if (order == null || order.getId() == null) {
            throw new IllegalArgumentException("Wrong parameters");
        } else if (order.getGoods() == null) {
            throw new IllegalArgumentException("Wrong parameters");
        }
        OrderEntity orderEntity = findOrderById(order.getId());
        if (order.getStartDate() == null || order.getStartDate().getTime() < 1) {
            Set<GoodsEntity> oldGoods = orderEntity.getGoods();
            Set<GoodsEntity> oldGoodsForExclude = new HashSet<>();
            Set<Long> idsOfNewGoods = new HashSet<>();
            for (WebGood g : order.getGoods()) {
                if (g.getId() != null) {
                    idsOfNewGoods.add(g.getId());
                }
            }
            Set<GoodsEntity> newGoods = goodsService.getGoodsByIds(idsOfNewGoods);
            Set<GoodsEntity> newGoodsForInclude = new HashSet<>(newGoods);
            for (GoodsEntity g : oldGoods) {
                if (!newGoods.contains(g)) {
                    g.setDateReady(null);
                    g.setOrder(null);
                    oldGoodsForExclude.add(g);
                } else {
                    newGoodsForInclude.remove(g);
                }
            }
            goodsService.saveGoods(oldGoodsForExclude);
            for (GoodsEntity g : newGoodsForInclude) {
                g.setOrder(orderEntity);
                g.setDateReady(new Date());
            }
            goodsService.saveGoods(newGoodsForInclude);
            orderEntity = findOrderById(orderEntity.getId());
            orderEntity = computeDeliveryTimeAndMaxWeight(orderEntity);
            orderEntity = orderCrudRepository.save(orderEntity);
            return OrderConverter.orderToWebOrder(orderEntity);
        } else {
            throw new IllegalArgumentException("Can't set goods. Order is on delivery.");
        }
    }

    @Override
    @Transactional
    public List<WebWaypoint> getWaypointsList(WebOrder order) throws DataAccessException, IllegalArgumentException {
        if (order == null || order.getId() == null) {
            throw new IllegalArgumentException("Wrong parameters");
        }
        Set<WaypointsEntity> waypoints = waypointsService.getSetOfWaypointsByOrderId(order.getId());
        List<WebWaypoint> webWaypointList = new ArrayList<>(WaypointConverter.waypointsToWebWaypoints(waypoints));
        webWaypointList.sort((s1, s2) -> s1.getNumber() - s2.getNumber());
        return webWaypointList;
    }

    @Override
    @Transactional
    public OrderEntity computeDeliveryTimeAndMaxWeight(OrderEntity order) throws DataAccessException, IllegalArgumentException {
        Set<GoodsEntity> goods = order.getGoods();
        if (goods == null && order.getWaypoints() == null) {
            throw new IllegalArgumentException("Wrong parameter.");
        }
        Set<Long> waypointsIds = new HashSet<>();
        for (WaypointsEntity w : order.getWaypoints()) {
            waypointsIds.add(w.getId());
        }
        waypointsService.deleteByIds(waypointsIds);
        Set<WaypointsEntity> waypointsEntities = new HashSet<>();
        int i = 1;
        int maxWeight = 0;
        int timeForDelivery = 0;
        for (GoodsEntity good : goods) {
            WaypointsEntity waypointLoading = new WaypointsEntity();
            WaypointsEntity waypointUnloading = new WaypointsEntity();
            waypointLoading.setCity(good.getCityForLoading());
            waypointUnloading.setCity(good.getCityForUnloading());
            waypointLoading.setGood(good);
            waypointUnloading.setGood(good);
            waypointLoading.setNumber(i++);
            waypointUnloading.setNumber(i++);
            waypointLoading.setType(WaypointsType.LOADING);
            waypointUnloading.setType(WaypointsType.UNLOADING);
            waypointLoading.setOrder(order);
            waypointUnloading.setOrder(order);
            maxWeight = maxWeight + good.getWeight();
            timeForDelivery = timeForDelivery + 2;
            waypointsEntities.add(waypointLoading);
            waypointsEntities.add(waypointUnloading);
        }
        waypointsService.saveAll(waypointsEntities);
        order.setMaxWeight(maxWeight);
        order.setTimeForDelivery(timeForDelivery);
        return order;
    }

    @Override
    @Transactional
    public WebWaypoint setWaypointReached(WebWaypoint webWaypoint) throws DataAccessException, IllegalArgumentException {
        if (webWaypoint == null || webWaypoint.getId() == null) {
            throw new IllegalArgumentException("Wrong parameters.");
        }
        WaypointsEntity waypoint = waypointsService.findById(webWaypoint.getId());
        GoodsEntity good = goodsService.findById(waypoint.getGood().getId());
        if (waypoint.getType().equals(WaypointsType.LOADING)) {
            good.setDateLoaded(new Date());
        }
        if (waypoint.getType().equals(WaypointsType.UNLOADING)) {
            good.setDateUnloaded(new Date());
        }
        goodsService.saveGood(good);
        OrderEntity order = findOrderById(waypoint.getOrder().getId());
        if (waypoint.getNumber() == 1) {
            order.setStartDate(new Date());
        }
        if (waypointsService.getSetOfWaypointsByOrderId(order.getId()).size() == waypoint.getNumber()) {
            order.setEndDate(new Date());
            order.setTimeForDelivery(order.getEndDate().getHours() - order.getStartDate().getHours());
            DriverEntity driver = order.getDriver();
            driver.setStatus(DriverStatus.ON_REST);
        }
        order.getDriver().setCurrentCity(waypoint.getCity());
        orderCrudRepository.save(order);
        return WaypointConverter.waypointToWebWaypoint(waypointsService.saveWaypoint(waypoint));
    }

    @Override
    @Transactional
    public Set<WebTruck> getTrucksForOrder(WebOrder order) throws IllegalArgumentException, DataAccessException {
        if (order == null || order.getId() == null) {
            throw new IllegalArgumentException("Wrong parameters");
        }
        OrderEntity orderEntity = findOrderById(order.getId());
        if (orderEntity.getGoods().size() == 0) {
            throw new IllegalArgumentException("Can't find truck for order. There is no goods in order." );
        }
        Set<WebTruck> trucks = truckService.getReadyForOrderTrucks(orderEntity.getMaxWeight());
        return trucks;
    }

    @Override
    public Set<WebOrder> findOrdersByTruck(WebTruck truck) throws DataAccessException, IllegalArgumentException {
        if (truck == null || truck.getId() == null) {
            throw new IllegalArgumentException("Wrong parameters");
        }
        TruckEntity truckEntity = truckService.findById(truck.getId());
        Set<OrderEntity> orders = orderCrudRepository.findAllByMaxWeightIsLessThanEqualAndStartDateNull(truckEntity.getCapacity());
        return OrderConverter.ordersToWebOrders(orders);
    }

    @Override
    public Set<WebOrder> findOrdersByDriver(WebDriver driver) throws DataAccessException, IllegalArgumentException {
        if (driver == null || driver.getId() == null) {
            throw new IllegalArgumentException("Wrong parameters");
        }
        DriverEntity driverEntity = driverService.findById(driver.getId());
        int maxWorkTimeInOrder = maxHoursInMonth - getDriverHoursOnDutyInMonth(driverEntity);
        if (maxWorkTimeInOrder > 0) {
            Set<OrderEntity> orders = orderCrudRepository.findAllByTruck_CurrentCityAndTimeForDeliveryLessThanEqualAndStartDateNull(driverEntity.getCurrentCity(), maxWorkTimeInOrder);
            return OrderConverter.ordersToWebOrders(orders);
        } else {
            throw new IllegalArgumentException("Can't find orders for driver with id = " + driverEntity.getId() + ". maxHoursInMonth limit reached.");
        }
    }
}
