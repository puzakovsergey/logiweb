package com.javaschool.service;

import com.javaschool.entity.user.UserEntity;
import com.javaschool.entity.user.UserRole;
import com.javaschool.model.WebUser;
import org.springframework.dao.DataAccessException;

import java.util.List;
import java.util.Set;

public interface UserService {
    Set<WebUser> findAll() throws DataAccessException;
    UserEntity findById(Long id) throws DataAccessException;
    WebUser findByIdC(Long id) throws DataAccessException;
    WebUser newUser(WebUser user) throws DataAccessException, IllegalArgumentException;
    void deleteById(Long id) throws DataAccessException;
    WebUser update(WebUser user) throws DataAccessException;
    WebUser loginProcedure (String login, String password);

}
