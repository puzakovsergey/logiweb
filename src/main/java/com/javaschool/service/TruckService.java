package com.javaschool.service;

import com.javaschool.entity.TruckEntity;
import com.javaschool.model.WebTruck;
import org.springframework.dao.DataAccessException;

import java.util.Set;

public interface TruckService {
    Set<WebTruck> findAll() throws DataAccessException;
    Set<WebTruck> findByLicencePlate(String licencePlate) throws DataAccessException;
    Set<WebTruck> findBroken() throws DataAccessException;
    TruckEntity findById(Long id) throws DataAccessException;
    WebTruck findByIdC(Long id) throws DataAccessException;
    WebTruck newTruck(WebTruck truck) throws DataAccessException, IllegalArgumentException;
    void deleteById(Long id) throws DataAccessException;
    WebTruck update(WebTruck truck) throws DataAccessException, IllegalArgumentException;
    Set<WebTruck> getReadyForOrderTrucks(int capacity) throws DataAccessException;
    TruckEntity saveTruck(TruckEntity truck) throws DataAccessException;
}
