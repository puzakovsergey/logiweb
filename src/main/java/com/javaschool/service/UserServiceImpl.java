package com.javaschool.service;

import com.javaschool.dao.UserCrudRepository;
import com.javaschool.entity.user.MyUserPrincipal;
import com.javaschool.entity.user.UserEntity;

import com.javaschool.entity.user.UserRole;
import com.javaschool.model.WebUser;
import com.javaschool.service.utils.UserConverter;
import com.javaschool.service.utils.Utils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;
import java.util.Set;

@Service
@EnableJpaRepositories(basePackages = {"com.javaschool.dao"})
public class UserServiceImpl implements UserService, UserDetailsService {
    @Autowired
    private UserCrudRepository userCrudRepository;
    private final static Logger LOGGER = Logger.getLogger(UserServiceImpl.class);

    @Override
    @Transactional
    public UserDetails loadUserByUsername(String login) throws UsernameNotFoundException {
        UserEntity user = userCrudRepository.findUserEntityByLogin(login);
        if (user == null) {
            throw new UsernameNotFoundException(login);
        }
        return new MyUserPrincipal(user);
    }

    @Override
    @Transactional
    public Set<WebUser> findAll() throws DataAccessException {
        Set<WebUser> users = UserConverter.usersToWebUsers(userCrudRepository.findAll());
        LOGGER.info("All users found");
        return users;
    }

    @Override
    @Transactional
    public UserEntity findById(Long id) throws DataAccessException {
        Optional<UserEntity> user = userCrudRepository.findById(id);
        LOGGER.info("User with id=" + id + " found");
        return user.get();
    }

    @Override
    @Transactional
    public WebUser findByIdC(Long id) throws DataAccessException {
        WebUser user = UserConverter.userToWebUser(userCrudRepository.findById(id).orElse(new UserEntity()));
        LOGGER.info("User with id=" + id + " found");
        return user;
    }

    @Override
    @Transactional
    public WebUser newUser(WebUser user) throws DataAccessException, IllegalArgumentException {
        if (user == null) {
            throw new IllegalArgumentException("User not defined");
        }
        UserEntity userEntity = new UserEntity();
        userEntity.setLogin(user.getLogin());
        userEntity.setPassword(user.getPassword());
        userEntity.setRole(user.getRole());
        return UserConverter.userToWebUser(userCrudRepository.save(userEntity));
    }

    @Override
    @Transactional
    public void deleteById(Long id) throws DataAccessException {
        userCrudRepository.deleteById(id);
        LOGGER.info("User with id=" + id + " deleted");
    }

    @Override
    @Transactional
    public WebUser update(WebUser user) throws DataAccessException {
        WebUser webUser = new WebUser();
        if (user == null) {
            return webUser;
        }
        if (user.getId() == null) {
            throw new IllegalArgumentException("Id not found");
        }
        UserEntity userEntity = findById(user.getId());
        if (user.getLogin() != null && !user.getLogin().equals("")) {
            userEntity.setLogin(user.getLogin());
        }
        if (user.getPassword() != null) {
            userEntity.setPassword(user.getPassword());
        }
        if (user.getRole() != null) {
            userEntity.setRole(user.getRole());
        }
        return UserConverter.userToWebUser(userCrudRepository.save(userEntity));
    }

    @Override
    @Transactional
    public WebUser loginProcedure (String login, String password) throws DataAccessException, SecurityException {
        UserEntity user = userCrudRepository.findUserEntityByLogin(login);
        if (user.getPassword().equals(password)) {
            return UserConverter.userToWebUser(user);
        }
        else {
            throw new SecurityException();
        }
    }
}
