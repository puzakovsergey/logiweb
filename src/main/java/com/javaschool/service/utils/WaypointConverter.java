package com.javaschool.service.utils;

import com.javaschool.entity.order.WaypointsEntity;
import com.javaschool.entity.order.WaypointsType;
import com.javaschool.model.WebWaypoint;

import java.util.Collection;
import java.util.HashSet;
import java.util.Set;

public class WaypointConverter {
    public static WebWaypoint waypointToWebWaypoint(WaypointsEntity waypoint) {
        WebWaypoint webWaypoint = new WebWaypoint();
        if (waypoint == null) {
            return webWaypoint;
        }
        webWaypoint.setId(waypoint.getId());
        webWaypoint.setCity(waypoint.getCity());
        webWaypoint.setGoodId(waypoint.getGood().getId());
        webWaypoint.setNumber(waypoint.getNumber());
        webWaypoint.setOrderId(waypoint.getOrder().getId());
        webWaypoint.setType(waypoint.getType());
        if (waypoint.getType() == WaypointsType.LOADING && waypoint.getGood().getDateLoaded() != null) {
            webWaypoint.setReached(true);
        }
        if (waypoint.getType() == WaypointsType.UNLOADING && waypoint.getGood().getDateUnloaded() != null) {
            webWaypoint.setReached(true);
        }
        return webWaypoint;
    }

    public static Set<WebWaypoint> waypointsToWebWaypoints(Set<WaypointsEntity> waypoints) {
        Set<WebWaypoint> webWaypoints = new HashSet<>();
        if (waypoints == null) {
            return webWaypoints;
        }
        for (WaypointsEntity w : waypoints) {
            if (w != null) {
                webWaypoints.add(waypointToWebWaypoint(w));
            }
        }
        return webWaypoints;
    }
}
