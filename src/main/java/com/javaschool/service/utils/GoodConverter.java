package com.javaschool.service.utils;

import com.javaschool.entity.TruckEntity;
import com.javaschool.entity.order.goods.GoodsEntity;
import com.javaschool.model.WebGood;
import com.javaschool.model.WebTruck;

import java.util.HashSet;
import java.util.Set;

public class GoodConverter {
    public static WebGood goodToWebGood(GoodsEntity good) {
        WebGood webGood = new WebGood();
        if (good == null) {
            return webGood;
        }
        if (good.getOrder() == null) {
            webGood.setOrderId(0L);
        } else {
            webGood.setOrderId(good.getOrder().getId());
        }
        webGood.setId(good.getId());
        webGood.setCityForLoading(good.getCityForLoading());
        webGood.setCityForUnloading(good.getCityForUnloading());
        webGood.setName(good.getName());
        webGood.setWeight(good.getWeight());
        webGood.setDateReady(good.getDateReady());
        webGood.setNotes(good.getNotes());
        return webGood;
    }

    public static Set<WebGood> goodsToWebGoods(Set<GoodsEntity> goods){
        Set<WebGood> webGoods = new HashSet<>();
        if (goods == null) {
            return webGoods;
        }
        for (GoodsEntity g : goods) {
            if (g != null) {
                webGoods.add(goodToWebGood(g));
            }
        }
        return webGoods;
    }

    public static GoodsEntity webGoodToGood(WebGood good) {
        if (good == null || good.getName() == null || good.getCityForLoading() == null || good.getCityForUnloading() == null || good.getWeight() < 1) {
            throw new IllegalArgumentException("Bad parameters");
        }
        GoodsEntity goodsEntity = new GoodsEntity();
        goodsEntity.setName(good.getName());
        goodsEntity.setCityForLoading(good.getCityForLoading());
        goodsEntity.setCityForUnloading(good.getCityForUnloading());
        goodsEntity.setWeight(good.getWeight());
        goodsEntity.setNotes(good.getNotes());

        return goodsEntity;
    }
}
