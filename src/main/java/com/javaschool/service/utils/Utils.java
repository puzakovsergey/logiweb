package com.javaschool.service.utils;

import com.javaschool.entity.TruckEntity;
import com.javaschool.entity.driver.DriverEntity;
import com.javaschool.entity.order.OrderEntity;
import com.javaschool.entity.order.goods.GoodsEntity;
import com.javaschool.entity.user.UserEntity;
import com.javaschool.model.WebDriver;
import com.javaschool.model.WebGood;
import com.javaschool.model.WebOrder;
import com.javaschool.model.WebUser;

import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;

public final class Utils {
     public static  <T> Set<T> setFromIterable(Iterable<T> i) {
        HashSet<T> set = new HashSet<>();
        Iterator<T> it = i.iterator();
        while (it.hasNext()) {
            set.add(it.next());
        }
        return set;
    }


}
