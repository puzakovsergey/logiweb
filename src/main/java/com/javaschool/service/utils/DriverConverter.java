package com.javaschool.service.utils;

import com.javaschool.entity.driver.DriverEntity;
import com.javaschool.entity.driver.DriverStatus;
import com.javaschool.entity.geo.City;
import com.javaschool.model.WebDriver;

import java.util.HashSet;
import java.util.Set;

public class DriverConverter {
    public static WebDriver driverToWebDriver(DriverEntity driver) {
            WebDriver webDriver = new WebDriver();
            if (driver == null) {
                return webDriver;
            }

            webDriver.setId(driver.getId());
            webDriver.setUser(UserConverter.userToWebUser(driver.getUser()));

            if (driver.getOrder() == null) {
                webDriver.setOrderId(0L);
            } else {
                webDriver.setOrderId(driver.getOrder().getId());
            }
            webDriver.setFirstName(driver.getFirstName());
            webDriver.setLastName(driver.getLastName());
            webDriver.setStatus(driver.getStatus());
            webDriver.setCurrentCity(driver.getCurrentCity());
            webDriver.setBirthDate(driver.getBirthDate());
            return webDriver;
    }

    public static Set<WebDriver> driversToWebDrivers(Set<DriverEntity> drivers) {
        Set<WebDriver> webDrivers = new HashSet<>();
        if (drivers == null) {
            return null;
        }
        for (DriverEntity d : drivers) {
            if (d != null) {
                webDrivers.add(driverToWebDriver(d));
            }
        }
        return webDrivers;
    }

    public static DriverEntity webDriverToDriver(WebDriver driver) throws IllegalArgumentException {
            if (driver == null && driver.getBirthDate() == null && driver.getLastName() == null && driver.getFirstName() == null) {
                throw new IllegalArgumentException("Bad parameters");
            }
            DriverEntity driverEntity = new DriverEntity();
            driverEntity.setFirstName(driver.getFirstName());
            driverEntity.setLastName(driver.getLastName());
            driverEntity.setStatus(DriverStatus.ON_REST);
            driverEntity.setBirthDate(driver.getBirthDate());
            if (driver.getCurrentCity() == null) {
                driverEntity.setCurrentCity(City.VORONEZH);
            } else {
                driverEntity.setCurrentCity(driver.getCurrentCity());
            }
            return driverEntity;
    }
}
