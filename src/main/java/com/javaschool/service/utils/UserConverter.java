package com.javaschool.service.utils;

import com.javaschool.entity.user.UserEntity;
import com.javaschool.entity.user.UserRole;
import com.javaschool.model.WebUser;

import java.util.HashSet;
import java.util.Set;

public class UserConverter {

    public static WebUser userToWebUser(UserEntity user) {
        WebUser webUser = new WebUser();
        if (user == null) {
            return webUser;
        }
        webUser.setId(user.getId());
        webUser.setLogin(user.getLogin());
        webUser.setPassword(user.getPassword());
        webUser.setRole(user.getRole());
        return webUser;
    }

    public static Set<WebUser> usersToWebUsers(Set<UserEntity> users){
        Set<WebUser> webUsers = new HashSet<>();
        if (users == null) {
            return webUsers;
        }
        for (UserEntity u : users) {
            if (u != null){
                webUsers.add(userToWebUser(u));
            }
        }
        return webUsers;
    }
}
